package protocols.hyparview;

import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.handlers.ProtocolRequestHandler;
import babel.requestreply.ProtocolRequest;
import protocols.hyparview.messages.DisconnectMessage;
import protocols.hyparview.messages.ForwardJoinMessage;
import protocols.hyparview.messages.JoinMessage;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import network.Host;
import network.INetwork;
import network.INodeListener;
import protocols.hyparview.replies.GetMembershipReply;
import protocols.hyparview.requests.GetMembershipReq;

import java.net.InetAddress;
import java.util.*;

public class HyParView extends GenericProtocol implements INodeListener {
    public final static short PROTOCOL_ID = 137;
    public final static String PROTOCOL_NAME = "HyParView";

    public short activeRandWalkLen;
    public short passiveRandWalkLen;
    private short fanout;
    private int passiveSizeLimit;
    private ArrayList<Host> activeView;
    private ArrayList<Host> passiveView;

    private Random rnd = new Random();

    public HyParView(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, HyParView.PROTOCOL_ID, net);

        //Requests
        registerRequestHandler(GetMembershipReq.REQUEST_ID, uponMembershipRequest);

        //Messages
        registerMessageHandler(JoinMessage.MSG_CODE, uponJoinMessage, JoinMessage.serializer);
        registerMessageHandler(ForwardJoinMessage.MSG_CODE, uponForwardJoinMessage, ForwardJoinMessage.serializer);
        registerMessageHandler(DisconnectMessage.MSG_CODE, uponDisconnectMessage, DisconnectMessage.serializer);
    }

    @Override
    public void init(Properties props) {
        registerNodeListener(this);
        fanout = Short.parseShort(props.getProperty("fanout", "3"));
        activeRandWalkLen = Short.parseShort(props.getProperty("ARWL", "3"));
        passiveRandWalkLen = Short.parseShort(props.getProperty("PRWL", "5"));
        short passiveRatio = Short.parseShort(props.getProperty("passive_ratio", "5"));
        passiveSizeLimit = (fanout + 1) * passiveRatio;
        passiveView = new ArrayList<>(passiveSizeLimit);
        activeView = new ArrayList<>(fanout + 1);

        if (props.containsKey("contact")) {
            try {
                String[] hostElems = props.getProperty("contact").split(":");
                Host contact = new Host(InetAddress.getByName(hostElems[0]), Short.parseShort(hostElems[1]));
                if (!contact.equals(myself)) {
                    addNetworkPeer(contact);
                    sendMessage(new JoinMessage(), contact);
                }
            } catch (Exception e) {
                System.err.println("Invalid contact on configuration: '" + props.getProperty("contact"));
            }
        }
    }


    private final ProtocolMessageHandler uponJoinMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            System.out.println("Join message received");
            JoinMessage message = (JoinMessage) msg;
            addNodeActiveView(message.getFrom());
            ForwardJoinMessage fjMessage = new ForwardJoinMessage(activeRandWalkLen);
            for (Host host : activeView) {
                if (!host.equals(message.getFrom())){
                    sendMessage(fjMessage, host);
                }
            }
        }
    };

    private final ProtocolMessageHandler uponForwardJoinMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            ForwardJoinMessage message = ((ForwardJoinMessage) msg);
            System.out.println(message);
            if (message.getTTL() == 0 || activeView.size() <= 1) {
                activeView.add(message.getFrom());
                JoinMessage joinMessage = new JoinMessage();
                sendMessage(joinMessage, message.getFrom());
            } else {
                if (message.getTTL() == passiveRandWalkLen) {
                    addNodePassiveView(message.getFrom());
                }
                // Pick index from available - 1
                int rndHostIndex = rnd.nextInt(activeView.size() - 1);
                Host chosenOne = activeView.get(rndHostIndex);
                short decreasedTTL = (short) (message.getTTL() - 1);
                if (chosenOne.equals(myself)) {
                    // Use neglected index if chosen index happens to be this host
                    sendMessage(new ForwardJoinMessage(decreasedTTL), activeView.get(activeView.size() - 1));
                } else {
                    sendMessage(new ForwardJoinMessage(decreasedTTL), chosenOne);
                }
            }
        }
    };

    private final ProtocolMessageHandler uponDisconnectMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            System.out.println("Disconnect message received");
            DisconnectMessage message = ((DisconnectMessage) msg);
            Host originator = message.originator;
            if (activeView.contains(originator)) {
                activeView.remove(originator);
                addNodePassiveView(originator);
            }
        }
    };

    private final ProtocolRequestHandler uponMembershipRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest req) {
            System.out.println("uponMembershipRequest");
            GetMembershipReq request = ((GetMembershipReq) req);
            //Send reply
            GetMembershipReply reply = new GetMembershipReply(request.getIdentifier(), new HashSet<>(activeView));
            reply.invertDestination(req);
            try {
                sendReply(reply);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };

    private void dropRandomElementFromActiveView() {
        Host host = activeView.remove(rnd.nextInt(activeView.size()));
        removeNetworkPeer(host);
        super.sendMessage(new DisconnectMessage(myself), host);
    }

    private void addNodeActiveView(Host node) {
        if (node != myself && !activeView.contains(node)) {
            if (activeView.size() == fanout + 1) {
                dropRandomElementFromActiveView();
            }
            activeView.add(node);
            addNetworkPeer(node);
        }
    }

    private void addNodePassiveView(Host node) {
        if (node != myself && !activeView.contains(node) && !passiveView.contains(node)) {
            if (passiveView.size() == passiveSizeLimit) {
                passiveView.remove(rnd.nextInt(passiveView.size()));
            }
            passiveView.add(node);
        }
    }

    @Override
    public void nodeConnectionReestablished(Host peer) {
        System.out.println("connection reestablished: " + peer);
    }

    @Override
    public void nodeDown(Host host) {
        System.out.println("Node down: " + host);
        synchronized (this) {
            removeNetworkPeer(host);
            activeView.remove(host);
            passiveView.remove(host);
        }
    }

    @Override
    public void nodeUp(Host host) {
        System.out.println("Node up: " + host);
    }
}
