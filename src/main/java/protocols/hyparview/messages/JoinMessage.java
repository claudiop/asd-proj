package protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

public class JoinMessage extends ProtocolMessage {
    public final static short MSG_CODE = 205;


    public JoinMessage() {
        super(JoinMessage.MSG_CODE);
    }

    public static final ISerializer<JoinMessage> serializer = new ISerializer<JoinMessage>() {
        @Override
        public void serialize(JoinMessage m, ByteBuf out) {
        }

        @Override
        public JoinMessage deserialize(ByteBuf in) {
            return new JoinMessage();
        }

        @Override
        public int serializedSize(JoinMessage m) {
            return 0;
        }
    };
}
