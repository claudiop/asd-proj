package protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class DisconnectMessage extends ProtocolMessage {
    public final static short MSG_CODE = 204;
    public final Host originator;

    public DisconnectMessage(Host originator) {
        super(DisconnectMessage.MSG_CODE);
        this.originator = originator;
    }

    public static final ISerializer<DisconnectMessage> serializer = new ISerializer<DisconnectMessage>() {
        @Override
        public void serialize(DisconnectMessage m, ByteBuf out) {
            m.originator.serialize(out);
        }

        @Override
        public DisconnectMessage deserialize(ByteBuf in) throws UnknownHostException {
            return new DisconnectMessage(Host.deserialize(in));
        }

        @Override
        public int serializedSize(DisconnectMessage m) {
            return m.originator.serializedSize();
        }
    };
}