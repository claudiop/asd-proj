package protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class ForwardJoinMessage extends ProtocolMessage {
    public final static short MSG_CODE = 202;

    private short ttl;

    public ForwardJoinMessage(short ttl) {
        super(ForwardJoinMessage.MSG_CODE);
        this.ttl = ttl;
    }

    public short getTTL() {
        return ttl;
    }

    public void decreaseTTL() {
        ttl--;
    }

    @Override
    public String toString() {
        return "FJoin " + getFrom() + " - " + ttl;
    }

    public static final ISerializer<ForwardJoinMessage> serializer = new ISerializer<ForwardJoinMessage>() {
        @Override
        public void serialize(ForwardJoinMessage m, ByteBuf out) {
            out.writeShort(m.getTTL());
        }

        @Override
        public ForwardJoinMessage deserialize(ByteBuf in) throws UnknownHostException {
            short ttl = in.readShort();
            return new ForwardJoinMessage(ttl);
        }

        @Override
        public int serializedSize(ForwardJoinMessage m) {
            return Short.BYTES;
        }
    };
}
