package protocols.hyparview.replies;

import babel.requestreply.ProtocolReply;
import network.Host;
import protocols.hyparview.requests.GetMembershipReq;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class GetMembershipReply extends ProtocolReply {
    public static final short REPLY_ID = GetMembershipReq.REQUEST_ID;

    private final UUID requestID;
    private final Set<Host> activeView;

    public GetMembershipReply(UUID requestID, Set<Host> activeView) {
        super(GetMembershipReply.REPLY_ID);
        this.requestID = requestID;
        this.activeView = new HashSet<>(activeView);
    }

    public UUID getRequestID() {
        return requestID;
    }

    public Set<Host> getActiveView() {
        return this.activeView;
    }
}