package protocols.hyparview.requests;

import babel.requestreply.ProtocolRequest;

import java.util.UUID;

public class GetMembershipReq extends ProtocolRequest {

    public static final short REQUEST_ID = 69;
    private UUID identifier;

    public GetMembershipReq() {
        super(GetMembershipReq.REQUEST_ID);
        this.identifier = UUID.randomUUID();
    }

    public GetMembershipReq(UUID identifier) {
        super(GetMembershipReq.REQUEST_ID);
        this.identifier = identifier;
    }

    public UUID getIdentifier() {
        return identifier;
    }
}
