package protocols.punblicator.messages;

import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class PublishMessage {
    String topic;
    String message;

    public PublishMessage(String topic, String message) {
        this.topic = topic;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Topic: "+ topic + " Message: " + message;
    }

    public static final ISerializer<PublishMessage> serializer = new ISerializer<PublishMessage>() {
        @Override
        public void serialize(PublishMessage m, ByteBuf out) {
            byte[] topicBytes = m.topic.getBytes();
            byte[] messageBytes = m.message.getBytes();
            out.writeInt(topicBytes.length);
            out.writeInt(messageBytes.length);
            out.writeBytes(topicBytes);
            out.writeBytes(messageBytes);
        }

        @Override
        public PublishMessage deserialize(ByteBuf in) {
            byte[] topicBA = new byte[in.readInt()];
            byte[] messageBA = new byte[in.readInt()];
            in.readBytes(topicBA);
            in.readBytes(messageBA);
            String topic = new String(topicBA, StandardCharsets.UTF_8);
            String message = new String(messageBA, StandardCharsets.UTF_8);
            return new PublishMessage(topic, message);
        }

        @Override
        public int serializedSize(PublishMessage m) {
            return (2 * Integer.BYTES) + m.topic.getBytes().length + m.message.getBytes().length;
        }
    };
}
