package protocols.punblicator;

import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.exceptions.NotificationDoesNotExistException;
import babel.exceptions.ProtocolDoesNotExist;
import babel.handlers.ProtocolNotificationHandler;
import babel.handlers.ProtocolRequestHandler;
import babel.notification.INotificationConsumer;
import babel.notification.ProtocolNotification;
import babel.protocol.GenericProtocol;
import babel.requestreply.ProtocolRequest;
import io.netty.buffer.ByteBuf;
import network.INetwork;
import protocols.floodbr.FloodBroadcast;
import protocols.floodbr.notifications.BCastDeliver;
import protocols.floodbr.requests.BCastRequest;
import protocols.punblicator.messages.PublishMessage;
import protocols.punblicator.notifications.PBDeliver;
import protocols.punblicator.requests.PublishRequest;
import protocols.punblicator.requests.SubscribeRequest;
import protocols.punblicator.requests.UnsubscribeRequest;

import java.net.UnknownHostException;
import java.util.*;

import static io.netty.buffer.Unpooled.buffer;

public class Punblicator extends GenericProtocol implements INotificationConsumer {
    public final static short PROTOCOL_ID = 248;
    public final static String PROTOCOL_NAME = "Punblicator";

    private HashMap<String, LinkedList<String>> topics;

    public Punblicator(INetwork net) throws HandlerRegistrationException, NotificationDoesNotExistException, ProtocolDoesNotExist {
        super(Punblicator.PROTOCOL_NAME, Punblicator.PROTOCOL_ID, net);

        topics = new HashMap<>();

        registerRequestHandler(SubscribeRequest.REQUEST_ID, uponSubscribeRequest);
        registerRequestHandler(UnsubscribeRequest.REQUEST_ID, uponUnsubscribeRequest);
        registerRequestHandler(PublishRequest.REQUEST_ID, uponPublishRequest);
        registerNotificationHandler(FloodBroadcast.PROTOCOL_ID, BCastDeliver.NOTIFICATION_ID, uponBCastDeliver);
    }

    @Override
    public void init(Properties properties) {

    }

    private final ProtocolNotificationHandler uponBCastDeliver = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification protocolNotification) {
            System.out.println("uponBCastDeliver");
            BCastDeliver deliveryNotification = (BCastDeliver) protocolNotification;
            byte[] payload = deliveryNotification.getMessage();
            ByteBuf buf = buffer(payload.length);
            buf.writeBytes(payload);
            try {
                PublishMessage topicMessage = PublishMessage.serializer.deserialize(buf);
                String topicName = topicMessage.getTopic();
                String message = topicMessage.getMessage();
                LinkedList<String> topic = topics.get(topicName);
                if (topic == null) return;
                topic.add(message);
                deliverNotification(new PBDeliver(topicName, message));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    };

    private final ProtocolRequestHandler uponSubscribeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest req) {
            System.out.println("uponSubscribeRequest");
            SubscribeRequest request = ((SubscribeRequest) req);
            if (!topics.containsKey(request.getTopic())) {
                topics.put(request.getTopic(), new LinkedList<>());
            }
            System.out.println(getState());
        }
    };

    private final ProtocolRequestHandler uponUnsubscribeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest req) {
            System.out.println("uponUnsubscribeRequest");
            UnsubscribeRequest request = ((UnsubscribeRequest) req);
            String topic = request.getTopic();
            topics.remove(topic);
            System.out.println(getState());
        }
    };

    private final ProtocolRequestHandler uponPublishRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest req) {
            System.out.println("uponPublishRequest");
            PublishRequest request = ((PublishRequest) req);
            PublishMessage message = new PublishMessage(request.getTopic(), request.getMessage());
            ByteBuf buf = buffer(PublishMessage.serializer.serializedSize(message));
            PublishMessage.serializer.serialize(message, buf);
            byte[] payload = buf.array();
            BCastRequest bCastRequest = new BCastRequest(payload);
            bCastRequest.setDestination(FloodBroadcast.PROTOCOL_ID);
            try {
                sendRequest(bCastRequest);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
            }
        }
    };

    public Set<String> getTopics() {
        return topics.keySet();
    }

    public List<String> getTopicMessages(String topic) {
        return topics.get(topic);
    }

    public String getState() {
        StringBuilder state = new StringBuilder();
        for (Map.Entry<String, LinkedList<String>> topic : topics.entrySet()) {
            state.append("Topic: ").append(topic.getKey()).append("\n");
            for (String message : topic.getValue()) {
                state.append("\t").append(message).append("\n");
            }
        }
        return state.toString();
    }

    @Override
    public void deliverNotification(ProtocolNotification notification) {
        BCastDeliver deliver = (BCastDeliver) notification;
        ByteBuf buf = buffer(0);
        buf.writeBytes(deliver.getMessage());
        try {
            PublishMessage message = PublishMessage.serializer.deserialize(buf);
            System.out.println("received event: " + message);
            synchronized (topics) {
                LinkedList<String> topic = topics.get(message.getTopic());
                if (topic != null) {
                    topic.add(message.getMessage());
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}