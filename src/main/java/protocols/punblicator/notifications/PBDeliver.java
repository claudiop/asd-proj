package protocols.punblicator.notifications;

import babel.notification.ProtocolNotification;

public class PBDeliver extends ProtocolNotification {
    public static final short NOTIFICATION_ID = 206;
    public static final String NOTIFICATION_NAME = "PBDeliver";

    private String topic;
    private String message;


    public PBDeliver(String topic, String message) {
        super(PBDeliver.NOTIFICATION_ID, PBDeliver.NOTIFICATION_NAME);
        if (topic == null) {
            throw new RuntimeException("Null topic");
        }
        if (message == null) {
            throw new RuntimeException("Null message");
        }

        this.topic = topic;
        this.message = topic;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }
}