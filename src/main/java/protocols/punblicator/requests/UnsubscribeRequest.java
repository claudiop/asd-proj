package protocols.punblicator.requests;

import babel.requestreply.ProtocolRequest;

public class UnsubscribeRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 201;

    private String topic;

    public UnsubscribeRequest(String topic) {
        super(UnsubscribeRequest.REQUEST_ID);
        if (topic == null) {
            throw new RuntimeException("Null topic");
        }
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public String toString() {
        return "Subscription request: Topic: " + topic;
    }
}
