package protocols.punblicator.requests;

import babel.requestreply.ProtocolRequest;

public class SubscribeRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 210;

    private String topic;

    public SubscribeRequest(String topic) {
        super(SubscribeRequest.REQUEST_ID);
        if (topic == null) {
            throw new RuntimeException("Null topic");
        }
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public String toString() {
        return "Subscription request: Topic: " + topic;
    }
}
