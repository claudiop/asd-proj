package protocols.punblicator.requests;

import babel.requestreply.ProtocolRequest;

public class PublishRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 212;

    private String topic;
    private String message;

    public PublishRequest(String topic, String message) {
        super(PublishRequest.REQUEST_ID);
        if (topic == null) {
            throw new RuntimeException("Null topic");
        }
        if (message == null) {
            throw new RuntimeException("Null message");
        }

        this.topic = topic;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }


    public String toString() {
        return "Publish request: Topic: "+ topic + " Message: " + message;
    }
}
