package protocols.floodbr;

import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.handlers.ProtocolReplyHandler;
import babel.handlers.ProtocolRequestHandler;
import babel.handlers.ProtocolTimerHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolReply;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import protocols.floodbr.messages.*;
import protocols.floodbr.notifications.BCastDeliver;
import protocols.floodbr.requests.BCastRequest;
import protocols.floodbr.timers.ConsistencyCheckTimer;
import protocols.floodbr.timers.ClearMessagesTimer;
import protocols.floodbr.timers.PullTimer;
import protocols.hyparview.HyParView;
import protocols.hyparview.replies.GetMembershipReply;
import protocols.hyparview.requests.GetMembershipReq;

import java.util.*;

public class FloodBroadcast extends GenericProtocol {

    public final static short PROTOCOL_ID = 200;
    public final static String PROTOCOL_NAME = "Flood Broadcast W/ Recovery";

    //Protocol State
    private Map<UUID, Long> delivered;
    private Map<UUID, ContentMessage> pending;
    private FloodType floodType;
    private boolean check;
    private long lastStateCheck;
    private int membershipCacheTTL;
    private long lastMembershipReply = 0;
    private Set<Host> membership;


    public FloodBroadcast(INetwork net) throws HandlerRegistrationException {
        super("FloodBroadcast", PROTOCOL_ID, net);

        //Register Events
        //Requests
        registerRequestHandler(BCastRequest.NOTIFICATION_ID, uponBCastRequest);

        //Replies
        registerReplyHandler(GetMembershipReply.REPLY_ID, uponGetMembershipReply);

        //Notifications Produced
        registerNotification(BCastDeliver.NOTIFICATION_ID, BCastDeliver.NOTIFICATION_NAME);

        //Messages
        registerMessageHandler(ContentMessage.MSG_CODE, uponContentMessage, ContentMessage.serializer);
        registerMessageHandler(LazyMessage.MSG_CODE, uponLazyMessage, ContentMessage.serializer);
        registerMessageHandler(PullMessage.MSG_CODE, uponPullMessage, ContentMessage.serializer);
        registerMessageHandler(StateCheckMessage.MSG_CODE, uponStateCheckProtocolMessage, StateCheckMessage.serializer);

        //Timers
        registerTimerHandler(PullTimer.TIMER_ID, uponPullTimer);
        registerTimerHandler(ConsistencyCheckTimer.TimerCode, uponCheckTimer);
        registerTimerHandler(ClearMessagesTimer.TimerCode, uponClearMessagesTimer);
    }

    @Override
    public void init(Properties props) {
        // Load parameters
        switch (props.getProperty("gossip_flood_type", "eager")) {
            case "lazy":
                this.floodType = FloodType.LAZY;
                break;
            case "pull":
                this.floodType = FloodType.PULL;
                break;
            default:
                this.floodType = FloodType.EAGER;
        }

        // Initialize State
        this.delivered = new HashMap<>();
        this.pending = new HashMap<>();
        this.membership = new HashSet<>();

        this.membershipCacheTTL = Integer.parseInt(props.getProperty("membership_cache_ttl", "0"));

        //setup timers
        if (floodType == FloodType.PULL) {
            int period = Integer.parseInt(props.getProperty("pull_poll_rate", "1000"));
            setupPeriodicTimer(new PullTimer(), period, period);
        }
    }

    private ProtocolTimerHandler uponPullTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer protocolTimer) {
            System.out.println("uponPullTimer");
            GetMembershipReq request = new GetMembershipReq();
            request.setDestination(HyParView.PROTOCOL_ID);
            try {
                sendRequest(request);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };

    private ProtocolRequestHandler uponBCastRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest r) {
            System.out.println("uponBCastRequest");
            //Create Message
            BCastRequest req = (BCastRequest) r;
            UUID mid = UUID.randomUUID();
            ContentMessage message = new ContentMessage(mid, req.getPayload());
            synchronized (this) {
                triggerNotification(new BCastDeliver(req.getPayload()));
                delivered.put(mid, System.currentTimeMillis());
                pending.put(mid, message);
                if (lastMembershipReply + membershipCacheTTL > System.currentTimeMillis()) {
                   sendPending();
                } else {
                    if (floodType != FloodType.PULL) {
                        GetMembershipReq request = new GetMembershipReq(mid);
                        request.setDestination(HyParView.PROTOCOL_ID);
                        try {
                            sendRequest(request);
                        } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                            destinationProtocolDoesNotExist.printStackTrace();
                            System.exit(1);
                        }
                    }
                }
            }
        }
    };

    private ProtocolReplyHandler uponGetMembershipReply = new ProtocolReplyHandler() {
        @Override
        public void uponReply(ProtocolReply reply) {
            System.out.println("uponGetMembershipReply");
            GetMembershipReply rep = (GetMembershipReply) reply;
            synchronized (membership) {
                membership = rep.getActiveView();
                lastMembershipReply = System.currentTimeMillis();
            }
            if (check) {
                check = false;
                for (Host h : rep.getActiveView()) {
                    sendMessage(new StateCheckMessage(delivered), h);
                }
            } else {
                ContentMessage msg = pending.get(rep.getRequestID());
                if(msg == null)System.out.println("empty");
                for (Host h : rep.getActiveView()) {
                    System.out.println(h.getPort());
                    sendMessage(msg, h);
                }
            }
        }
    };

    private ProtocolMessageHandler uponContentMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage m) {
            System.out.println("uponContentMessage");
            ContentMessage msg = (ContentMessage) m;
            synchronized (delivered) {
                //check if message was already observed, in that case ignore
                if (!delivered.containsKey(msg.getMessageId())) {
                    //Deliver message
                    delivered.put(msg.getMessageId(), System.currentTimeMillis());
                    BCastDeliver deliver = new BCastDeliver(msg.getPayload());
                    triggerNotification(deliver);

                    //Create Request for peers (in the membership)
                    GetMembershipReq request = new GetMembershipReq(msg.getMessageId());
                    request.setDestination(HyParView.PROTOCOL_ID);
                    pending.put(msg.getMessageId(), msg);
                    try {
                        sendRequest(request);
                    } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                        destinationProtocolDoesNotExist.printStackTrace();
                        System.exit(1);
                    }
                }
            }
        }
    };

    private ProtocolMessageHandler uponLazyMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage m) {
            System.out.println("uponLazyMessage");
            LazyMessage msg = (LazyMessage) m;
            synchronized (delivered) {
                if (!delivered.containsKey(msg.getMessageId())) {
                    sendMessage(new PullMessage(msg.getMessageId()), msg.getFrom());
                }
            }
        }
    };

    private ProtocolMessageHandler uponPullMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage m) {
            PullMessage pullMessage = (PullMessage) m;
            synchronized (pending) {
                ContentMessage contentMessage = pending.get(pullMessage.getMessageId());
                if (contentMessage == null) {
                    System.out.println("Feels bad");
                } else {
                    sendMessage(contentMessage, pullMessage.getFrom());
                }
            }
        }
    };

    private ProtocolTimerHandler uponCheckTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            synchronized (this) {
                check = true;
                GetMembershipReq request = new GetMembershipReq();
                request.setDestination(HyParView.PROTOCOL_ID);
                try {
                    sendRequest(request);
                } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                    destinationProtocolDoesNotExist.printStackTrace();
                    System.exit(1);
                }
            }
        }
    };
    private ProtocolTimerHandler uponClearMessagesTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            synchronized (this) {
                for (Map.Entry<UUID, Long> e : delivered.entrySet()) {
                    if (e.getValue() > lastStateCheck + 5000L) {
                        UUID mid = e.getKey();
                        pending.remove(mid);
                        delivered.remove(mid);
                    }
                }
                lastStateCheck = System.currentTimeMillis();
            }
        }
    };

    private ProtocolMessageHandler uponStateCheckProtocolMessage = new ProtocolMessageHandler() {

        @Override
        public void receive(ProtocolMessage m) {
            StateCheckMessage msg = (StateCheckMessage) m;
            Host h = msg.getFrom();
            for (UUID mid : delivered.keySet()) {
                if (!msg.getMids().containsKey(mid)) {
                    sendMessage(pending.get(mid), h);
                }
            }
            for (UUID mid : msg.getMids().keySet()) {
                if (!delivered.containsKey(mid)) {
                    sendMessage(new PullMessage(mid), h);
                }
            }
        }
    };


    private void sendPending() {
        synchronized (this) {
            for (ContentMessage message : pending.values()) {
                switch (floodType) {
                    case EAGER:
                        for (Host h : membership) {
                            sendMessage(message, h);
                        }
                        pending.remove(message.getMessageId());
                        break;
                    case LAZY:
                        for (Host h : membership) {
                            sendMessage(new LazyMessage(message.getMessageId()), h);
                        }
                        pending.remove(message.getMessageId());
                }
            }
        }
    }
}
