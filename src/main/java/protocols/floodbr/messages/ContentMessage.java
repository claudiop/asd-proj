package protocols.floodbr.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.UUID;

public class ContentMessage extends ProtocolMessage {

    public final static short MSG_CODE = 182;

    private final UUID mid;
    private byte[] payload;

    public ContentMessage(UUID mid, byte[] payload) {
        super(ContentMessage.MSG_CODE);
        this.mid = mid;
        this.payload = payload;
    }

    public void setPayload(byte[] payload) {
        this.payload = new byte[payload.length];
        System.arraycopy(payload, 0, this.payload, 0, payload.length);
    }

    public int getLength() {
        return this.payload.length;
    }

    public byte[] getPayload() {
        return this.payload;
    }

    public UUID getMessageId() {
        return mid;
    }

    public static final ISerializer<ContentMessage> serializer = new ISerializer<ContentMessage>() {
        @Override
        public void serialize(ContentMessage m, ByteBuf out) {
            out.writeLong(m.mid.getMostSignificantBits());
            out.writeLong(m.mid.getLeastSignificantBits());
            out.writeInt(m.payload.length);
            out.writeBytes(m.payload);
        }

        @Override
        public ContentMessage deserialize(ByteBuf in) throws UnknownHostException {
            UUID mid = new UUID(in.readLong(), in.readLong());
            int size = in.readInt();
            byte[] payload = new byte[size];
            in.readBytes(payload);
            return new ContentMessage(mid, payload);
        }

        @Override
        public int serializedSize(ContentMessage m) {
            return (2 * Long.BYTES) + Integer.BYTES + m.payload.length;
        }
    };
}
