package protocols.floodbr.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.util.UUID;

public class PullMessage extends ProtocolMessage {
    public final static short MSG_CODE = 180;
    private final UUID mid;


    public PullMessage(UUID requestedUUID) {
        super(PullMessage.MSG_CODE);
        this.mid = requestedUUID;
    }

    public UUID getMessageId() {
        return mid;
    }

    public static final ISerializer<PullMessage> serializer = new ISerializer<PullMessage>() {
        @Override
        public void serialize(PullMessage m, ByteBuf out) {
            out.writeLong(m.mid.getMostSignificantBits());
            out.writeLong(m.mid.getLeastSignificantBits());
        }

        @Override
        public PullMessage deserialize(ByteBuf in) {
            UUID mid = new UUID(in.readLong(), in.readLong());
            return new PullMessage(mid);
        }

        @Override
        public int serializedSize(PullMessage m) {
            return (2 * Long.BYTES);
        }
    };
}
