package protocols.floodbr.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.util.UUID;

public class LazyMessage extends ProtocolMessage {

    public final static short MSG_CODE = 181;

    private final UUID mid;


    public LazyMessage(UUID uuid) {
        super(LazyMessage.MSG_CODE);
        this.mid = uuid;
    }

    public UUID getMessageId() {
        return mid;
    }

    public static final ISerializer<LazyMessage> serializer = new ISerializer<LazyMessage>() {
        @Override
        public void serialize(LazyMessage m, ByteBuf out) {
            out.writeLong(m.mid.getMostSignificantBits());
            out.writeLong(m.mid.getLeastSignificantBits());
        }

        @Override
        public LazyMessage deserialize(ByteBuf in) {
            UUID mid = new UUID(in.readLong(), in.readLong());
            return new LazyMessage(mid);
        }

        @Override
        public int serializedSize(LazyMessage m) {
            return (2 * Long.BYTES);
        }
    };
}
