package protocols.floodbr.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class StateCheckMessage extends ProtocolMessage {

    public final static short MSG_CODE = 210;

    private Map<UUID, Long> mids;

    public StateCheckMessage(Map<UUID, Long> m) {
        super(StateCheckMessage.MSG_CODE);
        this.mids = m;
    }

    public Map<UUID, Long> getMids() {
        return mids;
    }

    public static final ISerializer<StateCheckMessage> serializer = new ISerializer<StateCheckMessage>() {
        @Override
        public void serialize(StateCheckMessage m, ByteBuf out) {
            out.writeInt(m.mids.values().size());
            for (UUID mid : m.mids.keySet()) {
                out.writeLong(mid.getMostSignificantBits());
                out.writeLong(mid.getLeastSignificantBits());
            }
        }

        @Override
        public StateCheckMessage deserialize(ByteBuf in) {
            int size = in.readInt();
            Map<UUID, Long> mids = new HashMap<>();
            while (size > 0) {
                UUID mid = new UUID(in.readLong(), in.readLong());
                mids.put(mid, 0L);
                size--;
            }
            return new StateCheckMessage(mids);
        }

        @Override
        public int serializedSize(StateCheckMessage m) {
            return Integer.BYTES + m.mids.values().size() * Long.BYTES * 2;
        }
    };
}
