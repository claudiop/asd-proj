package protocols.floodbr;

public enum FloodType {
    EAGER, LAZY, PULL
}
