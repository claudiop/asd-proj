package protocols.floodbr.requests;

import babel.requestreply.ProtocolRequest;

public class BCastRequest extends ProtocolRequest {

    public static final short NOTIFICATION_ID = 208;

    private byte[] payload;

    public BCastRequest(byte[] message) {
        super(BCastRequest.NOTIFICATION_ID);
        if(message != null) {
            this.payload = new byte[message.length];
            System.arraycopy(message, 0, this.payload, 0, message.length);
        } else {
            this.payload = new byte[0];
        }
    }

    public byte[] getPayload() {
        return payload;
    }
}
