package protocols.floodbr.timers;

import babel.timer.ProtocolTimer;

public class PullTimer extends ProtocolTimer {
    public final static short TIMER_ID = 174;

    public PullTimer() {
        super(PullTimer.TIMER_ID);
    }

    @Override
    public Object clone() {
        return null;
    }
}
