package protocols.floodbr.timers;

import babel.timer.ProtocolTimer;

public class ClearMessagesTimer extends ProtocolTimer {

    public static final short TimerCode = 556;

    public ClearMessagesTimer() {
        super(ClearMessagesTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}
