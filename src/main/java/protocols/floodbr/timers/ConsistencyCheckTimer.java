package protocols.floodbr.timers;

import babel.timer.ProtocolTimer;

public class ConsistencyCheckTimer extends ProtocolTimer {

    public static final short TimerCode = 555;

    public ConsistencyCheckTimer() {
        super(ConsistencyCheckTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}

