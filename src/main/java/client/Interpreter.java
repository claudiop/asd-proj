package client;

import babel.Babel;
import protocols.floodbr.FloodBroadcast;
import protocols.hyparview.HyParView;
import babel.notification.INotificationConsumer;
import babel.notification.ProtocolNotification;
import network.INetwork;
import protocols.punblicator.Punblicator;
import protocols.punblicator.notifications.PBDeliver;
import protocols.punblicator.requests.PublishRequest;
import protocols.punblicator.requests.SubscribeRequest;
import protocols.punblicator.requests.UnsubscribeRequest;

import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Interpreter extends Thread implements INotificationConsumer {
    private final static String regexGrammar = "" +
            "(subscribe (?<subTopic>\\S+)" +
            "|unsubscribe (?<unsubTopic>\\S+)" +
            "|publish (?<pubTopic>\\S+) (?<pubMessage>.*$)" +
            "|list" +
            "|exit)";
    private final static Pattern grammar = Pattern.compile(regexGrammar);
    private Punblicator punblicator;

    public Interpreter(Punblicator punblicator) {
        this.punblicator = punblicator;
    }

    @Override
    public void run() {
        super.run();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String line = scanner.nextLine();
            Matcher matcher = grammar.matcher(line);
            if (matcher.find()) {
                switch (line.split(" ")[0]) {
                    case "exit": {
                        return;
                    }
                    case "subscribe": {
                        String topic = matcher.group("subTopic");
                        SubscribeRequest request = new SubscribeRequest(topic);
                        punblicator.deliverRequest(request);
                        break;
                    }
                    case "unsubscribe": {
                        String topic = matcher.group("unsubTopic");
                        UnsubscribeRequest request = new UnsubscribeRequest(topic);
                        punblicator.deliverRequest(request);
                        break;
                    }
                    case "publish": {
                        String topic = matcher.group("pubTopic");
                        String message = matcher.group("pubMessage");
                        PublishRequest request = new PublishRequest(topic, message);
                        punblicator.deliverRequest(request);
                    }
                    case "list":
                        System.out.println(punblicator.getState());
                }
            } else {
                System.out.println("Bad command");
            }
        }
    }

    @Override
    public void deliverNotification(ProtocolNotification notification) {
        PBDeliver deliver = (PBDeliver) notification;
        System.out.println("Topic: " + deliver.getTopic() + "\n|---" + deliver.getMessage());
    }

    public static void main(String[] args) throws Exception {
        Babel babel = Babel.getInstance();
        Properties configProps;
        if (args.length == 0) {
            configProps = babel.loadConfig("network_config.properties", args);
        } else if (args.length == 1) {
            configProps = babel.loadConfig(args[0], new String[0]);
        } else {
            throw new Exception("I didn't quite like your arguments");
        }
        INetwork net = babel.getNetworkInstance();

        //Define protocols
        HyParView hpv = new HyParView(net);
        hpv.init(configProps);
        babel.registerProtocol(hpv);
        FloodBroadcast fBCast = new FloodBroadcast(net);
        fBCast.init(configProps);
        babel.registerProtocol(fBCast);
        Punblicator punblicator = new Punblicator(net);
        punblicator.init(configProps);
        babel.registerProtocol(punblicator);

        //Register protocols

        //subscribe to notifications
        Interpreter interpreter = new Interpreter(punblicator);
        interpreter.start();

        //start babel runtime
        babel.start();
        interpreter.join();
        System.exit(0);
    }


}
